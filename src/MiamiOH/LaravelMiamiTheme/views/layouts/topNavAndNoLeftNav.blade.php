@include("MiamiTheme::partials.top")
@include("MiamiTheme::partials.topNav")
<div class="row">
    <div class="col-xs-12">
        @yield('logOut')
        @yield('content')
    </div>
</div>
@include("MiamiTheme::partials.bottom")
