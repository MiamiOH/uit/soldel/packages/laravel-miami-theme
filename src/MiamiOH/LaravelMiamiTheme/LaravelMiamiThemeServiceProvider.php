<?php

namespace MiamiOH\LaravelMiamiTheme;

use Illuminate\Support\ServiceProvider;

class LaravelMiamiThemeServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/views', 'MiamiTheme');

        $this->publishes([
            __DIR__ . '/views' => resource_path('views/vendor/MiamiTheme'),
        ], 'views');

        $this->publishes([
            __DIR__ . '/assets' => public_path('vendor/MiamiTheme'),
        ], 'assets');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
