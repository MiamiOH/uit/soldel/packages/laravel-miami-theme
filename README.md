# Miami Theme for Laravel
This package contains views and assets that provide a uniform look and feel for Laravel applications.

## CSS Documentation
[https://webdev.admin.miamioh.edu/phpapps/miami-bootstrap3/](https://webdev.admin.miamioh.edu/phpapps/miami-bootstrap3/)

## First Time Installation
The Miami  theme package is added to your Laravel project as a dependency. This should be done a single time via composer, just like any other dependency. Once completed, the normal composer install/update commands should manage the theme files for you.

### Add the Satis Repo
The Miami Theme package is managed via GitLab and published on our Satis server. If necessary, add the satis repo to your composer.json file:

```
    "repositories": [
        {
            "type": "composer",
            "url": "https://satis.itapps.miamioh.edu/miamioh"
        }
    ]
```

### Require the Theme Package
The theme is added to your project using the normal composer require command:
```
$ composer require miamioh/laravel-miami-theme 5.*
```

### Add the Service Provider
You must tell Laravel about the new theme. Open ```config/app.php``` and add this to your providers key:
```
MiamiOH\LaravelMiamiTheme\LaravelMiamiThemeServiceProvider::class,
```

### Publish the Vendor Assets
Laravel themes publish web assets to public/vendor for the web server to access. Publish the assets by running:
```
php artisan vendor:publish --tag assets 
```

### Ignore the Vendor Assets
The published theme assets should **not** be included in git. Make sure to add the following to your git ignore file:
```
/public/vendor
```

### Enable Post Install/Update Scripts
Composer can run arbitrary scripts after completing install or update commands. Rather then manually publishing theme assets, you should add post install and update script to your composer.json file. The laraval initialization process creates a composer file with some scripts already configured. Adding new ones is easy. Locate the scripts block and add artisan vendor:publish commands:
```json
    "scripts": {
        "post-root-package-install": [
            "php -r \"file_exists('.env') || copy('.env.example', '.env');\""
        ],
        "post-create-project-cmd": [
            "php artisan key:generate"
        ],
        "post-install-cmd": [
            "Illuminate\\Foundation\\ComposerScripts::postInstall",
            "php artisan optimize",
            "php artisan vendor:publish --tag assets"
        ],
        "post-update-cmd": [
            "Illuminate\\Foundation\\ComposerScripts::postUpdate",
            "php artisan optimize",
            "php artisan vendor:publish --tag assets --force"
        ]
    },
```

**Note:** The --force option is necessary on the post update command so that the publish overwrites existing files with fresh, update-to-date copies.

## Getting Updates to the Theme
Once you have completed the set up of the theme in your project, you should be able to simply use composer to install the theme in a freshly cloned project or get updates to the theme. The composer install and update commands will correctly manage the dependency and publish the assets.

Since the build process uses composer to fetch dependencies, the theme assets will also be correctly included in the final package for deployment.

## Enhancing the Theme
We expect the theme to evolve not only to keep up with current Miami branding, but also add capabilities. Everyone is responsible for such changes. Use the GitLab issue feature to record changes and manage branches for work to be reviewed and merged.

Remember that we all benefit from improvements made to the core theme package rather than isolating them in a project.

## Usage

To use a theme template, you create a new view in resources/views and @extend one of the available layout templates.  To access the Miami Theme templates, you must use the MiamiTheme:: namespace, such as in:
```
@extends('MiamiTheme::layouts.noNav')
```

Within your view, you then add the various @sections to place in your content.  The two required sections are @section('title') to place the page's title and @section('content') for the page's main content, which is where you place your page's content.

### View Variables

Consider setting up a view composer in a service provider (such as app\Providers\AppServiceProvider.php or other custom service provider) to always pass in values without having to do so in your controller.  Example:

```
view()->composer('MiamiTheme::layouts.noNav', function ($view) {
    $view->with('appName', "Course List")
         ->with('pageTitle', "Search Page");
});
```
* $appName - This is the name of the application. 
* $pageTitle - This is for the title of the page displayed on the page itself; see yields for actual title

### Side Navigation Menu

A full featured menu solution has been added to the theme package using:

https://github.com/artkonekt/menu

Previously, we used lavary/laravel-menu to create the menu. However, it is no longer supported by Laravel 9 version, so it has been replaced with konekt/menu, which is a reworked version of Lavary Menu.
Follow the getting started directions (and the installation steps for Laravel < 9.0):

https://github.com/artkonekt/menu#getting-started

#### Upgrade to 5.* from previous versions

If your application is using AppNavMenu, you need to replace this old `lavary/laravel-menu` code with the `konekt/menu` code. 
In most of our Laravel apps, this code is located in the `App\Http\Middleware\GenerateMenus` file.

lavary/laravel-menu code:
```
\Menu::make('AppNavMenu', function ($menu) {
  $menu->add('Home', ['route' => 'test']);
  $menu->add('Office', ['url' => '/office]);
});
```

You must create a menu named `AppNavMenu` at this time.

Here is the menu we created for an application for example. It includes filtering items based on user permissions.

konekt/menu code:
```
 $navbar = Menu::create('AppNavMenu', ['share' => true]);

// Simple link to '/' via the URL helper
$navbar->addItem('Home', 'home', '/');
        
// Named route
$navbar->addItem('clients', 'Clients', ['route' => 'client.index']);

// Named route with parameter
$navbar->addItem('my-profile', 'My Profile', ['route' => ['user.show', 'id' => Auth::user()->id]]);

// Refer to an action
$navbar->addItem('projects', 'Projects', ['action' => 'ProjectController@index']);

// Action with parameter
$navbar->addItem('issue7', 'Issue 7', ['action' => ['IssueController@edit', 'id' => 7]]);

//Get items with a specific meta data:
$navbar->addItem('location', 'Location', ['route' => 'location'])->data('permission', 'manage-location');

//Filter menu items by a using filter() method
$navbar->items = $navbar->items->filter(function (Item $item) {
 if (empty($item->data('permission'))) {
   return true;
 }
 if (Auth::check()) {
   return Auth::user()->can($item->data('permission'));
 }
 return false;
});


```

If you intend to use an authorization check, as in the above example, and your application uses CAS for authentication, you must ensure that the CAS middleware is executed before the Menu middleware. You can do this by setting the middelware priority in your `app\Http\Kernel.php` file:

```
protected $middlewarePriority = [
    \App\Http\Middleware\MiamiCASAuth::class,
    \App\Http\Middleware\GenerateMenus::class,
];
```

Note that the `Auth::check()` method requires an `Authenticatable` object and that the `can()` method requires an `Authorizable` object. The simple `GenericUser` often used with CAS does implement those interfaces.

### Navigation Items (Legacy Menu)

* $sideNavItems - This array describes the side navigation items.  They are described in an array as such as the following.  ```name``` is displayed text, ```url``` is URL).

```
[
    ['name' => 'Home', 'url' => url('/')],
    ['name' => 'Admin', 'url' => url('/admin')],
    ['name' => 'Job Status', 'url' => url('/jobStatus')],
]
```

* $topNavItems - This array describes the top navigation items.  They are described in an array as such as the following.  ```name``` is displayed text, ```url``` is URL, and ```colspan``` is number of grid columns.

```
[
    ['name' => 'Home', 'url' => url('/')], 'colspan' => 2,
    ['name' => 'Admin', 'url' => url('/admin'), 'colspan' => 1],
    ['name' => 'Job Status', 'url' => url('/jobStatus')], 'colspan' => 3,
]
```

### Common Yields
* @yield('title') - the page's ```<title>```
* @yield('css') - Any custom CSS, placed within ```<head>``` 
* @yield('content') - Your page's main content, placed within the ```<body>``` after template header and navigation
* @yield('javascript') - Any custom JavaScript, placed before ```</body>```


### Optional Yields
* @yield('topLeftContent') - Content that should go in the left column above the navigation on the side
* @yield('bottomLeftContent') - Content that should go in the left column below the navigation on the side
* @yield('logOut') - Content that is to go at the top of the Content section (Often the location of the logout button, hence the name of the yield) 

### Available templates
* noNav - a basic page with no navigation elements; requires $pageTitle
* topNavAndLeftNav - a basic page with a top and left navigation; requires $pageTitle, $sideNavItems, and $topNavItems
* leftNavAndNoTopNav - a basic page with a left navigation and no top navigation; requires $pageTitle and $sideNavItems;
* topNavAndNoLeftNav - a basic page with a top navigation and no left navigation; requires $pageTitle and $topNavItems;

## Guide: How to Modify Bootstrap 3 Version of Miami Theme
All Miami theme related colors and css classes (e.g. `Miami Red #c3142d`,
`.miami-bordered`, `.nav-miami-vertical-justified`, .etc) are declared and implemented 
in the [miami-bootstrap3](https://git.itapps.miamioh.edu/solution-delivery-dev/miami-bootstrap3) project. Any change made to
items above should apply directly in the [miami-bootstrap3](../miami-bootstrap3),
re-generate new minified bootstrap3.min.css and replace existing one in the
`src/MiamiOH/LaravelMiamiTheme/assets/css` directory. See README file 
in the [miami-bootstrap3](https://git.itapps.miamioh.edu/solution-delivery-dev/miami-bootstrap3) project for more details.

Notes:  
- Do not add classes declaration of any (additional) Miami theme element in the 
`bootstrap_stype.css` stylesheet. It is ONLY used to provide styles for 
layout templates (e.g. nonav, leftNav, etc)
- Every time `bootstrap.min.css` and `bootstrap_style.min.css` being modified,
version number in the `src/MiamiOH/LaravelMiamiTheme/views/partials/top.blade.php`
should also be properly updated/incremented in order to avoiding browser cache of old
version stylesheet.

## Todo
* Additional page templates
* Additional UCM components
